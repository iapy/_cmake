include(CMakePackageConfigHelpers)
find_package(Python COMPONENTS Interpreter)

macro(InstallPackage package)
    set(INSTALL_PATH "share/cmake/${package}")

    configure_package_config_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/cmake/${package}Config.cmake.in"
        "${CMAKE_CURRENT_BINARY_DIR}/cmake/${package}Config"
        INSTALL_DESTINATION ${INSTALL_PATH})

    install(DIRECTORY ${PROJECT_SOURCE_DIR}/include/ DESTINATION include ${ARGN})
    if(EXISTS ${PROJECT_BINARY_DIR}/generated AND "${ARGN}" STREQUAL "")
        install(DIRECTORY ${PROJECT_BINARY_DIR}/generated/ DESTINATION include ${ARGN})
    endif()

    install(EXPORT "${package}Targets" FILE "${package}Targets.cmake"
        NAMESPACE ${package}::
        DESTINATION ${INSTALL_PATH})

    install(FILES
        "${CMAKE_CURRENT_BINARY_DIR}/cmake/${package}Config"
        DESTINATION ${INSTALL_PATH} RENAME ${package}Config.cmake)

    export(PACKAGE ${package})
endmacro()

function(InstallDependencies)
    if(EXISTS ${CMAKE_INSTALL_PREFIX}/.installer-state)
        execute_process(COMMAND ${Python_EXECUTABLE} ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/packager.py
            -G "${CMAKE_GENERATOR}" ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_INSTALL_PREFIX} ${WITHOUT})
        file(GLOB_RECURSE installed_packages_ "${CMAKE_INSTALL_PREFIX}/*/cmake/*[Cc]onfig.cmake")
    else()
        execute_process(COMMAND ${Python_EXECUTABLE} ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/packager.py
            -G "${CMAKE_GENERATOR}" ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR}/packages ${WITHOUT})
        file(GLOB_RECURSE installed_packages_ "${CMAKE_CURRENT_BINARY_DIR}/packages/*/cmake/*[Cc]onfig.cmake")
    endif()
    foreach(pkg_ ${installed_packages_})
        get_filename_component(p_ ${pkg_} DIRECTORY)
        get_filename_component(n_ ${p_} NAME)
        set(${n_}_DIR ${p_})
    endforeach()

    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${Python_EXECUTABLE} ${CMAKE_CURRENT_FUNCTION_LIST_DIR}/compiler.py")
endfunction()

option(BUILD_TESTS "Build tests" ON)
option(WITHOUT "Without packages" "")

if("${CMAKE_BUILD_TYPE}" STREQUAL "")
    set(CMAKE_BUILD_TYPE "Release")
endif()

list(PREPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR})
list(PREPEND CMAKE_PREFIX_PATH ${PROJECT_BINARY_DIR}/packages/lib/cmake)
list(PREPEND CMAKE_PREFIX_PATH ${PROJECT_BINARY_DIR}/packages/lib64/cmake)
list(PREPEND CMAKE_PREFIX_PATH ${PROJECT_BINARY_DIR}/packages/share/cmake)

message(STATUS "Build type ${CMAKE_BUILD_TYPE}")
