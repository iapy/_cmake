import os, os.path, sys
from argparse import ArgumentParser
from subprocess import Popen
import atexit
import multiprocessing

p = ArgumentParser()
p.add_argument('-G', dest='generator', type=str)
p.add_argument('project', type=os.path.abspath)
p.add_argument('packages', type=os.path.abspath)

args, without = p.parse_known_args()
state = os.path.join(args.packages, '.installer-state')

make = ['make']
jobs = os.environ.get('MAKE_PARALLELISM', str(multiprocessing.cpu_count()))

if args.generator == 'Unix Makefiles':
    make = ['make', f'-j{jobs}']
elif args.generator == 'Ninja':
    make = ['ninja', f'-j{jobs}']

installed = []

def install_local(p, _):
    if os.path.basename(p[0]) in without:
        return
    if os.path.exists(os.path.join(p[0], 'requirements.txt')):
        install_requirements(os.path.join(p[0], 'requirements.txt'))
    name = os.path.basename(p[0])
    build = os.path.join(args.packages, 'local', name + '.build')
    os.makedirs(build, exist_ok=True)
    Popen(['cmake', f'-G{args.generator}', f'-DCMAKE_INSTALL_PREFIX={args.packages}', '-DBUILD_TESTS=0', p[0]] + p[1:], cwd=build).wait()
    return build

def install_github(p, base):
    if '@' in p[0]:
        repo, commit = p[0].split('@')
        depth = []
    else:
        repo, commit = p[0], 'master'
        depth = ['--depth', '1']

    name = os.path.basename(repo)
    if name in without:
        return

    source = os.path.join(args.packages, 'github', repo)
    os.makedirs(source, exist_ok=True)
    if not os.path.exists(os.path.join(source, '.git')):
        Popen(['git', 'clone'] + depth + ['https://github.com/' + repo, source]).wait()
        if commit != 'master':
            Popen(['git', 'checkout', '-b', commit, commit], cwd=source).wait()

        if os.path.exists(os.path.join(source, '.gitmodules')):
            Popen(['git', 'submodule', 'init'], cwd=source).wait()
            Popen(['git', 'submodule', 'update'], cwd=source).wait()

        pf = os.path.join(base, os.path.basename(repo).lower() + '.patch')
        if os.path.exists(pf):
            print(f'Patching with {pf}', file=sys.stderr)
            Popen(['patch', '-p1', '-i', pf], cwd=source).wait()
    build = source + '.build'
    os.makedirs(build, exist_ok=True)
    Popen(['cmake', f'-G{args.generator}', f'-DCMAKE_INSTALL_PREFIX={args.packages}', source] + p[1:], cwd=build).wait()
    return build

def install_requirements(requirements):
    this = os.path.dirname(requirements)
    with open(requirements, 'r') as rf:
        for r in [l.strip().split() for l in rf]:
            p = os.path.normpath(os.path.join(os.path.dirname(__file__), r[0]))
            if os.path.isdir(p):
                r[0] = p
                f = install_local
            else:
                f = install_github

            with open(state, 'r') as s:
                installed = [l.strip() for l in s]
            if r[0] in installed:
                continue

            with open(state, 'a') as s:
                print(r[0], file=s)
            print(f'Building package {r[0]} <- {this}', file=sys.stderr)
            build = f(r, this)
            if build is not None:
                print(f'Installing package {r[0]}', file=sys.stderr)
                Popen(make, cwd=build).wait()
                Popen(make + ['install'], cwd=build).wait()

if __name__ == '__main__':
    os.makedirs(os.path.join(args.packages, 'lib', 'cmake'), exist_ok=True)
    if not os.path.exists(state):
        with open(state, 'w') as s:
            print(args.project, file=s)
        def delete_installer_state():
            os.remove(state)
        atexit.register(delete_installer_state)
    install_requirements(os.path.join(args.project, 'requirements.txt'))
