import sys
import queue

from threading import Thread
from subprocess import Popen, PIPE

HOST="cg::Host<"
GRAPH="cg::Graph<"
WITH="[with "
SERVICE="cg::Service<{"
DATABASE="jsondb::Server<"

def cleanup(line, sub, pair):
    incr = dict((c, 1 - 2*i) for i, c in enumerate(pair))
    index = line.find(sub)
    while index >= 0:
        replace = index + len(sub)
        balance = 1
        for i in range(replace, len(line)):
            balance += incr.get(line[i], 0)
            if balance == 0:
                break
        line = line[0:replace] + '...' + line[i:]
        index = line.find(sub, replace)
    return line

def reader(pipe, queue):
    try:
        with pipe:
            for line in iter(pipe.readline, b''):
                line = line.decode('utf-8').rstrip()
                line = cleanup(line, HOST,     '<>')
                line = cleanup(line, GRAPH,    '<>')
                line = cleanup(line, WITH,     '[]')
                line = cleanup(line, SERVICE,  '{}')
                line = cleanup(line, DATABASE, '<>')
                queue.put(line)
    finally:
        queue.put(None)

p, q = Popen(sys.argv[1:], stderr=PIPE), queue.Queue()
Thread(target=reader, args=[p.stderr, q]).start()

for line in iter(q.get, None):
    print(line)

p.wait()
exit(p.returncode)
